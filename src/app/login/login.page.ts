import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private authService: AuthService,
    private navCtrl: NavController
  ) { }

  ngOnInit() {
    const subscription = this.authService.getUser().subscribe(user => {
      if (user !== null) {
        this.navCtrl.navigateRoot(['/home']);
        subscription.unsubscribe();
      }
    });
  }

  public async login() {
    await this.authService.login();
  }
}
