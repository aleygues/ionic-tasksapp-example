import { Component, OnDestroy } from '@angular/core';
import { TasksService, Task } from '../services/tasks.service';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  public tasks$: Observable<Task[]>;

  constructor(
    private service: TasksService,
    private authService: AuthService,
    private navCtrl: NavController
  ) {
    this.tasks$ = this.service.getTasks();
  }

  public async logout() {
    await this.authService.logout();
    this.navCtrl.navigateRoot(['/login']);
  }
}
