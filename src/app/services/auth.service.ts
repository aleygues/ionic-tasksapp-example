import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { auth } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private firebaseAuth: AngularFireAuth
  ) { }

  public getUser(): Observable<any> {
    return this.firebaseAuth.user;
  }

  public async login() {
    await this.firebaseAuth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  public async logout() {
    await this.firebaseAuth.auth.signOut();
  }
}
