import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Task {
  id?: string;
  title: string;
  content: string;
}

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  constructor(
    private store: AngularFirestore
  ) { }

  public getTasks(): Observable<Task[]> {
    return this.store.collection('tasks').snapshotChanges().pipe(map(taskDocs => {
      return taskDocs.map(taskDoc => taskDoc.payload.doc.data());
    })) as any;
  }

  public async getTaskById(id: string): Promise<Task> {
    const doc = await this.store.collection('tasks').doc(id).get().toPromise();
    return doc.data() as Task;
  }

  public async createTask(task: Task): Promise<string> {
    const taskId = Date.now() + '-' + (Math.floor(Math.random() * 8999) + 1000);
    task.id = taskId;
    await this.store.collection('tasks').doc(taskId).set(task);
    return taskId;
  }

  public async deleteTask(task: Task): Promise<void> {
    await this.store.collection('tasks').doc(task.id).delete();
  }

  public async updateTask(task: Task): Promise<void> {
    await this.store.collection('tasks').doc(task.id).set(task);
  }
}
