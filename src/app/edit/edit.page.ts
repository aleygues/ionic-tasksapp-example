import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TasksService, Task } from '../services/tasks.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {
  public task: Task = {
    title: '',
    content: ''
  };

  constructor(
    private activeRoute: ActivatedRoute,
    private service: TasksService,
    private navCtrl: NavController
  ) { }

  async ngOnInit() {
    const taskId = this.activeRoute.snapshot.paramMap.get('id');
    if (taskId) {
      this.task = { ...await this.service.getTaskById(taskId) };
    }
  }

  public async save() {
    if (this.task.id) {
      await this.service.updateTask(this.task);
    } else {
      await this.service.createTask(this.task);
    }
    this.navCtrl.back();
  }
}
