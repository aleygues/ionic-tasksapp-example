import { Component, OnInit } from '@angular/core';
import { Task, TasksService } from '../services/tasks.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {
  public task: Task = { title: '', content: '' };

  constructor(
    private activeRoute: ActivatedRoute,
    private service: TasksService,
    private navCtrl: NavController
  ) {

  }

  ngOnInit() {

  }

  async ionViewWillEnter() {
    const taskId = this.activeRoute.snapshot.paramMap.get('id');
    this.task = await this.service.getTaskById(taskId);
  }

  public delete() {
    this.service.deleteTask(this.task);
    this.navCtrl.back();
  }
}
